#!/usr/bin/env python3
"""Builds packages"""
import sys
from pathlib import Path
from prebuilder.systems import CMake, Make
from prebuilder.distros.debian import Debian
from prebuilder.buildPipeline import BuildPipeline, BuildRecipy
from prebuilder.repoPipeline import RepoPipelineMeta
from prebuilder.core.Package import PackageMetadata
from prebuilder.core.Package import PackageRef, VersionedPackageRef
from prebuilder.fetchers.GitRepoFetcher import GitRepoFetcher
from fsutilz import movetree, copytree
from ClassDictMeta import ClassDictMeta

thisDir = Path(".").absolute()


class build(metaclass=RepoPipelineMeta):
	"""It's {maintainerName}'s repo for {repoKind} packages of build tools."""
	
	DISTROS = (Debian,)
	
	def pocl():
		#repoURI = "https://github.com/pocl/pocl"
		repoURI = "https://github.com/KOLANICH/pocl"
		
		bakeBuildRecipy = BuildRecipy(CMake, GitRepoFetcher(repoURI, refspec="llvm11"), buildOptions = {"POCL_ICD_ABSOLUTE_PATH": True, "ENABLE_RELOCATION": True, "ENABLE_SPIR": True, "ENABLE_LATEST_CXX_STD": True})
		bakeMetadata = PackageMetadata("pocl")
		
		return BuildPipeline(bakeBuildRecipy, ((Debian, bakeMetadata),))


if __name__ == "__main__":
	build()
